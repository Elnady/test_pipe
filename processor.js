const AWS = require('aws-sdk');
const gm = require('gm')
    .subClass({ imageMagick: true });
/*
    NOTE:
       Sharp is faster than GM, so it's used to resize images
*/

const sharp = require('sharp');

const config = require('config'),
    resizes = config.get('resizes'),
    max_age = config.get('CACHE_HEADER_MAX_AGE');
// apiServiceURL = config.get('apiService.url');

const apiAdapter = require('./config/api-adapter');

const s3 = new AWS.S3({
    accessKeyId: config.get('bucketCredentials.id'),
    secretAccessKey: config.get('bucketCredentials.secretKey')
});
/*
    NOTE: 
      GM .toBuffer() built-in function "yield empty buffer" if the image resultion 
      is great, so this function is implemented
*/
exports.convertToBuffer = function (data) {
    return new Promise((resolve, reject) => {

        data.stream((err, stdout, stderr) => {
            if (err) {
                console.log(">>> err from convertToBuffer >>>", err);
                return reject(err);
            }
            const chunks = [];
            stdout.on('data', (chunk) => {
                chunks.push(chunk);
            })

            stdout.once('end', () => {
                resolve(Buffer.concat(chunks))
            })

            stderr.once('data', (data) => {
                reject(String(data));
            })
        })
    })
}

// used Sharp Lib
function _sharpImageResize(data, sharpObj) {
    console.log(">>>> sharp Object to resize based on >>>>", sharpObj);


    return new Promise((resolve, reject) => {
        const imageToResize = data.Body ? data.Body : data;

        return sharp(imageToResize, { failOnError: false })
            .resize(sharpObj)
            .toBuffer()
            .then((buffer => {
                console.log(">>>>> data from sharp.toBuffer() >>>>", buffer);
                resolve({
                    resizedBuffer: buffer,
                    contentType: data.ContentType || 'image/jpeg'

                })
            }))
            .catch((err) => {
                console.log("#### error ####", err);
                reject(err);
            })
    })
}

// used Sharp Lib
function _GMImageResize(data, width, height, imageType) {
    return new Promise((resolve, reject) => {
        const imageToResize = data.Body ? data.Body : data;

        const streamedData = gm(imageToResize).resize(width, height)
        return exports.convertToBuffer(streamedData)
            .then((buffer) => {

                resolve({
                    resizedBuffer: buffer,
                    contentType: data.ContentType || 'image/jpeg'
                })
            })
            .catch((err) => {
                console.log("#### error ####", err);
                reject(err);
            })
    })
}

exports.s3Write = function (params) {
    params.CacheControl = `max-age=${max_age}`;
    console.log("... inside s3Write ...", params);
    return new Promise((resolve, reject) => {

        s3.putObject(params).promise().then((response) => {
            console.log(">>>>> output of writeHandler.js Function >>>>>", response);
            resolve(response);
        }).catch(err => {
            reject(err);
        })
    })
}

exports.s3Read = (params) => {
    return new Promise((resolve, reject) => {
        params.Bucket = `${params.Bucket}/images`;
        params.Key = `${params.Key}.${params.imageType}`
        delete params.imageType;
        delete params.watermarkedRequired

        s3.getObject(params).promise()
            .then(data => {
                console.log("### resolve msg from readHandler ###", data);
                resolve(data);
            })
            .catch(err => {
                console.log("### reject msg from readHandler ###", err);
                reject(err);
            })
    })
}

exports.asyncUpdateDataBase = (params, apiEnvURL) => {
    const apiService = apiAdapter(apiEnvURL);

    const { Key } = params;
    return new Promise((resolve, reject) => {
        // apiService.get('/get') // to test dummy api https://httpbin.org
        apiService.post('/api/v2/media-files/confirm', {
            lambdaTest: "00b76f40-8609-11e9-a637-4f3e7a24a42c",
            auth: "1234567890",
            key_name: Key
        })
            .then((response) => {
                resolve(response);
            })
            .catch((err) => {
                console.log(">>>>>", err);
            })
    })
}

// exports.updateDataBase = (params) => {
//     console.log(">>>> params passed to _updateDB function >>>>", params);

//     const { Key } = params;

//     apiService.post('/api/v2/media-files/confirm', {
//         lambdaTest: "00b76f40-8609-11e9-a637-4f3e7a24a42c",
//         auth: "1234567890",
//         key_name: Key
//     })
// }

exports.resize = async (image, srcKey, imageType, bucketName) => {

    const { width: originalWidth, height: originalHeight } = await sharp(image).metadata();
    console.log("??? metadata ???", originalWidth, originalHeight);
    const originalBiggerDim = Math.max(originalWidth, originalHeight);

    const sharpObj = {};
    (originalBiggerDim === originalWidth) ? sharpObj.width = 0 : sharpObj.height = 0;

    return new Promise((resolve, reject) => {

        const resizePromises = [];
        const uploadPromises = [];

        for (let resize of resizes) {
            // resizePromises.push(_sharpImageResize(image, resize.width))
            if (originalBiggerDim >= resize.MAX_DIM) {
                sharpObj[Object.keys(sharpObj)[0]] = resize.MAX_DIM;
                resizePromises.push(_sharpImageResize(image, sharpObj))
            }
        }

        Promise.all(resizePromises)
            .then(resizeRes => {
                if (resizeRes !== null) {
                    const { contentType } = resizeRes[0];

                    let params = {
                        Bucket: `${bucketName}/thumbs`,
                        ContentType: contentType,
                        ACL: 'public-read'
                    }


                    for (let i = 0; i < resizeRes.length; i++) {
                        params.Body = resizeRes[i].resizedBuffer;

                        let targetSizeName = resizes[i].name;
                        let resizedKey = `${targetSizeName}_${srcKey}.${imageType}`;
                        params.Key = resizedKey;

                        uploadPromises.push(exports.s3Write(params));
                    }

                    Promise.all(uploadPromises)
                        .then(data => {
                            console.log('AAAAAAAAAA', data);
                            resolve(data);
                        })
                        .catch((err) => {
                            console.log("qqqqqqqqq", err);
                        })
                }
            })
    })
}
