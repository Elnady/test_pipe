module.exports = {
    bucketCredentials: {
        id: 'AKIAUHWZFVWW6YHVLI7L',
        secretKey: '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'
    },
    bucketFolders: {
        originalFolder: 'images',
        watermarkedFolder: 'watermark',
        thumbsFolder: 'thumbs'
    },
    resizes: [{  //Todo: refactor this structure 
        name: 'normal',
        MAX_DIM: 2000,
        // height: 1706
    }, {
        name: '150',
        MAX_DIM: 150,
        // height: 150
    }, {
        name: '600',
        MAX_DIM: 600,
        // height: 600
    }, {
        name: '900',
        MAX_DIM: 900,
        // height: 900
    }],
    watermarkSettings: {
        imagePath: './logo.png',
        relativeSize: 50,
        opacity: 30
    },
    CACHE_HEADER_MAX_AGE: '2592000', // In milliseconds
    DEVELOPMENT: {
        apiService: {
            url: "https://api-v2.sprint.myzesty.com",
            // url: "https://httpbin.org",
        }
    },
    PRODUCTION: {
        apiService: {
            url: "https://api.myzesty.com",
            // url: "https://api.sprint.myzesty.com",
        }
    },
}