const sharp = require('sharp');
const config = require('config');
// const { convertToBuffer } = require('./processor')

module.exports = {

	async addWatermark(image) {
		try {
			console.log("$$$ image $$$", image);

			const relativeSize = config.get('watermarkSettings.relativeSize'),
				opacity = config.get('watermarkSettings.opacity');

			if (!image) {
				console.log("..... ")
			}

			const resizedWatermarkBuffer = await _resizeWatermarkImage(image, relativeSize);

			const watermarkedImage = await _applyWatermark(image, resizedWatermarkBuffer);
			console.log(">>> watermarkedImage >>>", watermarkedImage);

			return watermarkedImage;
		} catch (err) {
			console.log("/// err ///", err);
		}
	} // end addwatermark

} // end export 


async function _resizeWatermarkImage(image, relativeSize) { // ToDo: refactor 
	try {
		let resizedWatermarkBuffer;

		const metadata = await sharp(image.Body).metadata();
		console.log("||| metadata |||", metadata);

		const watermarkSide = Math.floor(Math.sqrt(metadata.width * metadata.height * (relativeSize / 100)));

		await sharp('logo.png')
			.resize(Math.floor(watermarkSide / 3.5))
			// .extend({
			// 	top: 0,
			// 	bottom: Math.floor(metadata.width * 0.05),
			// 	left: 0,
			// 	right: Math.floor(metadata.height * 0.05),
			// 	background: { r: 0, g: 0, b: 0, alpha: 0 }
			// })
			// .toFile('resized-logo.png');
			.toBuffer()
			.then((data) => {
				resizedWatermarkBuffer = data;
				console.log(">>> watermarkedBuffer >>>", resizedWatermarkBuffer);
			})
			.catch(err => {
				console.log(">>> ERR >>>", err);
			});

		return resizedWatermarkBuffer;
	} catch (err) {
		console.log("--- err ---", err);
	}

}

async function _applyWatermark(image, resizedWatermarkBuffer) { // image here is buffer 
	try {
		console.log("%%%%%%%%%%% resizedWatermarkBuffer %%%%%%%%%%%%", resizedWatermarkBuffer);
		let watermarkedBuffer,
			contentType;

		await sharp(image.Body)
			.composite([{
				input: resizedWatermarkBuffer,
				gravity: 'southeast'
				// blend: 'xor'
				// top: 10, 
				// left: 10
			}])
			.sharpen()
			.withMetadata()
			.toBuffer({ resolveWithObject: true })
			.then(({ data, info }) => {

				watermarkedBuffer = data;
				console.log(">>> watermarkedBuffer >>>", watermarkedBuffer);

				contentType = `image/${info.format}`;
				console.log(">>> contentType >>>", contentType);

			})
			.catch(err => {
				console.log(">>> ERR >>>", err);
			});

		return { watermarkedBuffer, contentType };

	} catch (err) {
		console.log("!!! err !!!", err);
	}

}







////////////////////////////////////////
///////////////////////////////////////
/// OLD RUNNING CODE BY GM 
///////////////////////////////////////
// const gm = require('gm')
// 	.subClass({ imageMagick: true });
// const config = require('config');
// const { convertToBuffer } = require('./processor')

// module.exports = {
// 	addWatermark(image) {
// 		return new Promise((resolve, reject) => {
// 			const relativeSize = config.get('watermarkSettings.relativeSize'),
// 				opacity = config.get('watermarkSettings.opacity');

// 			if (!image) {
// 				console.log("..... ")
// 			}
// 			return _resizeWatermarkImage(image, relativeSize)
// 				.then((responseData) => {
// 					const { image, geometry } = responseData;
// 					return _applyWatermark(image, geometry, opacity)
// 						.then((watermarkedImage) => {
// 							resolve(watermarkedImage)
// 						})
// 						.catch((err) => {
// 							console.log("000000000", err);
// 						})
// 				})
// 				.catch((err) => {
// 					console.log("!!!!!!!!!", err);
// 				})

// 		}) // end Promise
// 	}
// }

// function calculateGeometry(boundingBox, minSide) {
// 	return new Promise((resolve, reject) => {
// 		resolve(
// 			minSide + 'x' + minSide +  // minimum height & width
// 			'+0+0' + // offset
// 			'^' // Maintain aspect ratio
// 		)
// 	})
// }

// function _resizeWatermarkImage(image, relativeSize) { // ToDo: refactor 
// 	return new Promise((resolve, reject) => {
// 		// validate filepath is png 

// 		gm(image.Body)
// 			.size((err, size) => {
// 				const watermarkSide = Math.sqrt(size.width * size.height * (relativeSize / 100));

// 				return calculateGeometry(size, watermarkSide)
// 					.then((geometry) => {
// 						resolve({
// 							image,
// 							geometry
// 						})
// 					})
// 					.catch((err) => {
// 						console.log("xxxxxxx", err);
// 					})
// 			})
// 	});
// }

// function _applyWatermark(image, geometry, opacity) {
// 	return new Promise((resolve, reject) => {
// 		const streamedData = gm(image.Body)
// 			.composite(config.get('watermarkSettings.imagePath'))
// 			.geometry(geometry)
// 			.gravity('Center')
// 			.dissolve(opacity)

// 		return convertToBuffer(streamedData)
// 			.then((buffer) => {
// 				console.log(">>>>> new bufferRes >>>>", buffer);
// 				resolve({
// 					watermarkedBuffer: buffer,
// 					contentType: image.ContentType
// 				})
// 			})
// 			.catch((err) => {
// 				console.log("#### error ####", err);
// 				reject(err);
// 			})
// 	})
// }

////////////////////////////////////////
///////////////////////////////////////
/// OLD RUNNING CODE BY GM 
///////////////////////////////////////