const config = require('config');
const { s3Read, resize, s3Write, asyncUpdateDataBase, updateDataBase } = require('./processor');
const zestyMarker = require('./zestyMarker');

let apiEnvURL;

exports.handler = function (event, context, callback) {
    const arn = context.invokedFunctionArn,
        arnParts = arn.split(':'),
        processEnv = arnParts[arnParts.length - 1];

    console.log(">>>> arn >>>>", arn);
    console.log('---- processEnv ----', processEnv);

    apiEnvURL = config.get(`${processEnv}.apiService.url`);
    /////////////////////////////////////////////

    if (event.ping) return; // to not fail while warming

    const bucket = event.Records[0].s3.bucket.name;
    console.log(">>>> bucketName >>>>", bucket);
    const _originalKey = decodeURIComponent(event.Records[0].s3.object.key.replace(/\+/g, " "));

    const typeMatch = _originalKey.match(/\.([^.]*)$/);
    if (!typeMatch) {
        callback("Could not determine the image type.");
        return;
    }

    const imageType = typeMatch[1];
    console.log(">>> imageType after deploy >>>>", imageType);
    // if (imageType !== "jpg" && imageType !== "png" && imageType !== "jpeg") {
    //     callback(`Unsupported image type: ${imageType}`);
    //     return;
    // }

    let originalKey = _originalKey.split('/')[1].split('.')[0]; // get fileName without extension
    console.log(">>>>>>>>> ", originalKey);

    const params = {
        Bucket: bucket,
        Key: originalKey, // rawFileName or rawFileName_watermarked
        imageType,
        watermarkedRequired: originalKey.split('_')[1] === 'watermarked' ? true : false
    }

    console.log('>>>>> original params >>>>>', params)

    return processImages(params)
        .then(() => {
            console.log("LAMBDA FUNCTION IS EXCUTED SUCCESSFULLY.");

            // _updateDataBase(params);
            // context.succeed();
            return asyncUpdateDataBase(params, apiEnvURL)
                .then((apiRes) => {
                    console.log(">>>>>>>>>>>> @@@@@@@@@ >>>>>>>> ", apiRes);
                    context.succeed();
                })
        })
        .catch((error) => {
            console.log("Error : ", error);
            context.fail();
        })

};

function processImages(params) {
    return new Promise((resolve, reject) => {

        const bucketName = params.Bucket,
            imageType = params.imageType,
            watermarkedRequired = params.watermarkedRequired;

        let srcKey = params.Key;
        // remove "_watermarked" keyword from srcKey before handle it
        srcKey = srcKey.replace('_watermarked', '');

        return s3Read(params)
            .then((downloadedPic => {
                if (downloadedPic !== null) {

                    if (watermarkedRequired) {
                        console.log("inner if watermarkedRequired ")
                        return zestyMarker.addWatermark(downloadedPic)
                            .then((watermarkedImage) => {
                                const { watermarkedBuffer, contentType } = watermarkedImage;

                                let params = {
                                    Bucket: `${bucketName}/thumbs`,
                                    Body: watermarkedBuffer,
                                    ContentType: contentType,
                                    ACL: 'public-read'
                                }

                                let waterMarkedKey = `watermarked_${srcKey}.${imageType}`;

                                params.Key = waterMarkedKey;

                                s3Write(params)
                                    .then((data) => {
                                        console.log('@@@@@@', data);
                                        return resize(watermarkedBuffer, srcKey, imageType, bucketName)
                                            .then((data) => {
                                                resolve(data);
                                            })
                                    })
                                    .catch(err => {
                                        console.log(">>>>> err !!!", err);
                                    })
                            })
                            .catch((err) => {
                                console.log("Something went wrong in adding watermark !.")
                            })
                    }

                    return resize(downloadedPic, srcKey, imageType, bucketName)
                        .then((data) => {
                            resolve(data);
                        })
                }
            }))
            .catch(err => {
                console.log("...... err can not read file .....", err);
            })
    });
}
